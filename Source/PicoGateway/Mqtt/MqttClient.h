#ifndef MQTTCLIENT_H
#define MQTTCLIENT_H

// Qt includes -----------------------------
#include <QObject>
#include <QtMqtt/QMqttClient>

class MqttClient : public QObject
{
  Q_OBJECT

public:

  enum Enum_QOS
  {
    QOS_0 = 0, // Al massimo una volta, non c'e alcuna garanzia di consegna
    QOS_1 = 1, // Almeno una volta
    QOS_2 = 2  // Esattamente una volta
  };

  MqttClient(QObject *parent = nullptr);

  void Connect(const QString &hostname,
               int port);

  void Publish(const QString &topic,
               const QString &message,
               Enum_QOS qos);

private:

  QMqttClient m_QMqttClient;

};

#endif // MQTTCLIENT_H
