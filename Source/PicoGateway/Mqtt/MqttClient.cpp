
// File includes ---------------------------
#include "MqttClient.h"

// Qt includes -----------------------------
#include <QElapsedTimer>
#include <QCoreApplication>

//-----------------------------------------------------------------------------------------------------------------------------

MqttClient::MqttClient(QObject *parent)
  : QObject(parent)
  , m_QMqttClient(this)
{
}

//-----------------------------------------------------------------------------------------------------------------------------

void MqttClient::Connect(const QString &hostname,
                         int port)
{
  m_QMqttClient.setHostname(hostname);
  m_QMqttClient.setPort(port);

  m_QMqttClient.connectToHost();

  QElapsedTimer qElapsedTimer_Timeout;
  qElapsedTimer_Timeout.start();
  while(m_QMqttClient.state() != QMqttClient::Connected)
  {
    if(qElapsedTimer_Timeout.elapsed() > 5 * 1000)
      throw QString("Timeout connecting to mqtt broker at %1:%2").arg(hostname)
                                                                 .arg(port);

    QCoreApplication::processEvents();
  }
}

//-----------------------------------------------------------------------------------------------------------------------------

void MqttClient::Publish(const QString &topic,
                         const QString &message,
                         Enum_QOS qos)
{
  if(m_QMqttClient.publish(topic,
                           message.toUtf8(),
                           (quint8)qos)
     == -1)
    throw QString("Could not publish message on topic '%1'").arg(topic);
}
