QT -= gui
QT += mqtt

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Destination directory
DESTDIR = bin

# Generated files directory
OBJECTS_DIR = build
MOC_DIR = build
UI_DIR = build

INCLUDEPATH += \
                /opt/picoscope/include

LIBS += \
       /opt/picoscope/lib/libusbdrdaq.so


SOURCES += \
    Main/main.cpp \
    Main/MainClass.cpp \
    Mqtt/MqttClient.cpp \
    USBDrDaq/USBDrDaq.cpp

HEADERS += \
    Main/MainClass.h \
    Mqtt/MqttClient.h \
    USBDrDaq/USBDrDaq.h
