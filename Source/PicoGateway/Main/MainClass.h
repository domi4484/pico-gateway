#ifndef MAINCLASS_H
#define MAINCLASS_H

// Project includes ------------------------
#include "Mqtt/MqttClient.h"

// Qt includes -----------------------------
#include <QObject>

// Forward declarations --------------------
class USBDrDaq;
class QMqttSubscription;

class MainClass : public QObject
{
  Q_OBJECT

public:

  explicit MainClass(QObject *parent = nullptr);
  ~MainClass();

protected:

  virtual void timerEvent(QTimerEvent *);

private:

  // USBDrDaq
  USBDrDaq *m_USBDrDaq;
  
  // MqttClient
  MqttClient m_MqttClient;

  QMqttSubscription *m_QMqttSubscription_PicoscopeError;
  QMqttSubscription *m_QMqttSubscription_TemperaturaEsterna;
  QMqttSubscription *m_QMqttSubscription_TemperaturaInterna;
  QMqttSubscription *m_QMqttSubscription_UmiditaRelativaInterna;
  
  int m_Timer;

  void readAndSaveValuesFromDevice();

};

#endif // MAINCLASS_H
