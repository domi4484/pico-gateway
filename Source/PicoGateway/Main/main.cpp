
// Project includes ------------------------
#include "MainClass.h"

// Qt includes -----------------------------
#include <QCoreApplication>
#include <QDir>

int main(int argc, char *argv[])
{
  //Set the current directory to the current program path
  QDir::setCurrent(QFileInfo(argv[0]).absolutePath());

  QCoreApplication a(argc, argv);

  MainClass mainClass;

  return a.exec();
}
