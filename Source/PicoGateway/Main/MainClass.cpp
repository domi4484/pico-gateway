// Files includes --------------------------
#include "MainClass.h"

// Project includes ------------------------
#include "USBDrDaq/USBDrDaq.h"

// Qt includes -----------------------------
#include <QDebug>
#include <QFile>
#include <QCoreApplication>

//-----------------------------------------------------------------------------------------------------------------------------

MainClass::MainClass(QObject *parent)
  : QObject(parent)
  , m_USBDrDaq(nullptr)
  , m_MqttClient(this)
  , m_QMqttSubscription_PicoscopeError(nullptr)
  , m_QMqttSubscription_TemperaturaEsterna(nullptr)
  , m_QMqttSubscription_TemperaturaInterna(nullptr)
  , m_QMqttSubscription_UmiditaRelativaInterna(nullptr)
  , m_Timer(-1)
{
  // Application informations
  QCoreApplication::setApplicationName    ("PicoGateway");
  QCoreApplication::setApplicationVersion ("V0.0.4");

  qDebug() << (QString("Starting %1 %2").arg(QCoreApplication::applicationName()).arg(QCoreApplication::applicationVersion()));

  m_USBDrDaq = new USBDrDaq(this);

  try
  {
    m_USBDrDaq->Open();
  }
  catch(const QString &exception)
  {
    qDebug() << exception;
    exit(1);
  }

  try
  {
    m_MqttClient.Connect("localhost",
                         1883);
  }
  catch(const QString &exception)
  {
    qDebug() << exception;
    exit(1);
  }

  try
  {
    readAndSaveValuesFromDevice();
  }
  catch(const QString &exception)
  {
    qDebug() << exception;
    exit(1);
  }

  m_Timer = startTimer(60 * 1000);
}

//-----------------------------------------------------------------------------------------------------------------------------

MainClass::~MainClass()
{
  if(m_Timer != -1)
    killTimer(m_Timer);

  m_USBDrDaq->Close();
  delete m_USBDrDaq;
}

//-----------------------------------------------------------------------------------------------------------------------------

void MainClass::timerEvent(QTimerEvent *)
{
  try
  {
    readAndSaveValuesFromDevice();
  }
  catch(const QString &exception)
  {
    qDebug() << exception;
    try
    {
      m_MqttClient.Publish("sensors/picoscope/error",
                           exception,
                           MqttClient::QOS_0);
    }
    catch(const QString &exception)
    {
      qDebug() << exception;
    }
  }
}

//-----------------------------------------------------------------------------------------------------------------------------

void MainClass::readAndSaveValuesFromDevice()
{
  // Temperatura esterna
  {
    double temperaturaEsterna = m_USBDrDaq->ReadExternalSensor1();
    qDebug() << QString("Temperatura esterna: %1").arg(temperaturaEsterna, 0, 'f', 1);

    QFile qFile("TemperaturaEsterna");
    if (qFile.open(QFile::WriteOnly) == false)
      throw QString ("Can't open '%1' for writing").arg(qFile.fileName());

    qFile.write(QString("%1\n").arg(temperaturaEsterna, 0, 'f', 1).toUtf8());
    qFile.close();

    m_MqttClient.Publish("sensors/picoscope/temperature/esterno",
                         QString::number(temperaturaEsterna, 'f', 1),
                         MqttClient::QOS_0);
  }

  // Temperatura interna
  {
    double temperaturaInterna = m_USBDrDaq->ReadExternalSensor2();
    qDebug() << QString("Temperatura interna: %1").arg(temperaturaInterna, 0, 'f', 1);

    QFile qFile("TemperaturaInterna");
    if (qFile.open(QFile::WriteOnly) == false)
      throw QString ("Can't open '%1' for writing").arg(qFile.fileName());

    qFile.write(QString("%1\n").arg(temperaturaInterna, 0, 'f', 1).toUtf8());
    qFile.close();

    m_MqttClient.Publish("sensors/picoscope/temperature/interno",
                         QString::number(temperaturaInterna, 'f', 1),
                         MqttClient::QOS_0);
  }

  // Umidita relativa
  {
    double umiditaRelativa = m_USBDrDaq->ReadExternalSensor3();
    qDebug() << QString("Umidita relativa:    %1").arg(umiditaRelativa, 0, 'f', 1);

    QFile qFile("UmiditaRelativa");
    if (qFile.open(QFile::WriteOnly) == false)
      throw QString ("Can't open '%1' for writing").arg(qFile.fileName());

    qFile.write(QString("%1\n").arg(umiditaRelativa, 0, 'f', 1).toUtf8());
    qFile.close();

    m_MqttClient.Publish("sensors/picoscope/umidity/interno",
                         QString::number(umiditaRelativa, 'f', 1),
                         MqttClient::QOS_0);
  }
}

//-----------------------------------------------------------------------------------------------------------------------------
