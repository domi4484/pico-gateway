#ifndef USBDRDAQ_H
#define USBDRDAQ_H

// Qt includes -----------------------------
#include <QObject>

// Picoscope includes ----------------------
#include <libusbdrdaq-1.0/usbDrDaqApi.h>

class USBDrDaq : public QObject
{
    Q_OBJECT

public:
   
  USBDrDaq(QObject *parent = nullptr);
  ~USBDrDaq();

  void Open();
  void Close();

  double ReadExternalSensor1();
  double ReadExternalSensor2();
  double ReadExternalSensor3();

private:

  int16_t m_Handle;

  double collectBlockImmediate(USB_DRDAQ_INPUTS channel);
  double adcToMillivolts (double raw);

};
#endif // USBDRDAQ_H
